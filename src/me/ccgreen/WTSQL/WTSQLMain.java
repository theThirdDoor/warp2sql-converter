package me.ccgreen.WTSQL;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import me.ccgreen.SQLlib.SQLlibMain;

public class WTSQLMain extends JavaPlugin {

	public static int curID = 0;
	public static ConsoleCommandSender CONSOLE;

	public static HashMap<String, String> SAVED_WARPS = new HashMap<String, String>();

	public File warpsFile;
	private File playersFile;
	public YamlConfiguration warpsYml;
	public YamlConfiguration playersYml;
	private boolean warpsExists = false, playersExists = false;

	@Override
	public void onEnable() {
		SQLlibMain SQL = (SQLlibMain) Bukkit.getServer().getPluginManager().getPlugin("SQLlib");
		CONSOLE = Bukkit.getServer().getConsoleSender();
		if(SQL.safetyCheck()) {
			printInfo("Loading files");
			loadFiles();
			saveFiles(SQL);
		} else {
			printError("This plugin won't run until the config for SQLlib has been setup");
		}
	}

	@Override
	public void onDisable(){
	}

	public static void printInfo(String line){
		CONSOLE.sendMessage(ChatColor.GREEN + "[W2SQL] : " + line);
	}

	public static void printWarning(String line){
		CONSOLE.sendMessage(ChatColor.YELLOW + "[W2SQL] : " + line);
	}

	public static void printError(String line){
		CONSOLE.sendMessage(ChatColor.RED + "[W2SQL] : " + line);
	}

	public static String convertToMColors(String line){
		return line.replaceAll("&", "§");
	}

	private void loadFiles() {
		warpsFile = new File(getDataFolder(), "warps.yml");
		playersFile = new File(getDataFolder(), "players.yml");

		if(warpsFile.exists()){
			warpsExists = true;
			warpsYml = YamlConfiguration.loadConfiguration(warpsFile);
			curID = warpsYml.getInt("CurID");
		}
		if(playersFile.exists()){
			playersExists = true;
			playersYml = YamlConfiguration.loadConfiguration(playersFile);
			try {
				Set<String> playerList = playersYml.getConfigurationSection("Players").getKeys(false);
				for (String uuid : playerList) {
					List<String> knownWarps = playersYml.getStringList("Players." + uuid);
					String warps = null;
					for (String id : knownWarps) {
						warps = warps + "|" + id;
					}
					SAVED_WARPS.put(uuid, warps);
				}
			} catch (NullPointerException e) {
				//File doesn't exist yet
			}
		}

	}

	private void saveFiles(SQLlibMain SQL) {
		if(warpsExists) {
			printInfo("Begining uploading warps");
			//Init tables
			SQL.initialiseTable("rpwarps_playerwarps", "id", "id MEDIUMINT NOT NULL, owner TINYTEXT NOT NULL, name TEXT NOT NULL, world TINYTEXT NOT NULL, x MEDIUMINT NOT NULL, y SMALLINT NOT NULL, z MEDIUMINT NOT NULL, material TINYTEXT NOT NULL, durability SMALLINT NOT NULL, banned LONGTEXT");
			
			//Playerwarps
			printWarning("uploading player warps!");
			Vector<String> pWarpsBatch = new Vector<String>();
			Set<String> playerWarpList = warpsYml.getConfigurationSection("PlayerWarps").getKeys(false);
			for(String id : playerWarpList){
				String world, x, y, z, owner, name, material, durability, banned;
				world = warpsYml.getString("PlayerWarps." + id + ".Location.World").toString();
				x = "" + warpsYml.getDouble("PlayerWarps." + id + ".Location.X");
				y = "" + warpsYml.getDouble("PlayerWarps." + id + ".Location.Y");
				z = "" + warpsYml.getDouble("PlayerWarps." + id + ".Location.Z");
				owner = warpsYml.getString("PlayerWarps." + id + ".Owner");
				name = warpsYml.getString("PlayerWarps." + id + ".Name").replace("'", "");
				material = warpsYml.getString("PlayerWarps." + id + ".Material");
				durability = warpsYml.getString("PlayerWarps." + id + ".Durability");
				if(durability == null) {
					durability = "0";
				}
				banned = warpsYml.getStringList("PlayerWarps." + id + ".BannedUsers").toString();
				
				String warp = "REPLACE INTO rpwarps_playerwarps (id, world, x, y, z, owner, name, material, durability, banned) VALUES ('" + id + "', '" + world + "', '" + x + "', '" + y + "', '" + z + "', '" + owner + "', '" + name + "', '" + material + "', '" + durability + "', '" + banned + "');";
				
				int i = 0;
				pWarpsBatch.add(warp);
				if (++i % 1000 == 0) {
					printInfo("Sending player warps: " + 1000 * ((i/1000) - 1) + " - " + i + " of " + playerWarpList.size());
					SQL.sendBatchOnMainThread(pWarpsBatch);
					pWarpsBatch.clear();
				}
			}
			SQL.sendBatchOnMainThread(pWarpsBatch);
		}

		//Playerdata
		if(playersExists) {
			printInfo("Uploading player data");
			SQL.initialiseTable("rpwarps_playerData", "uuid", "uuid VARCHAR(37) NOT NULL, warps TEXT");

			int i = 0;
			Vector<String> batch = new Vector<String>();

			for (Entry<String, String> entry  : SAVED_WARPS.entrySet()) {
				String uuid = entry.getKey();
				String warps = playersYml.getStringList("Players." + uuid).toString();

				if(warps != "[]") {

					String pData = "REPLACE INTO rpwarps_playerData (uuid, warps) VALUES ('" + uuid + "', '" + warps + "');";
					batch.add(pData);
					if (++i % 1000 == 0) {
						printInfo("Sending player data: " + 1000 * ((i/1000) - 1) + " - " + i + " of " + SAVED_WARPS.size() + " (Max)");
						SQL.sendBatchOnMainThread(batch);
						batch.clear();
					}
				}
			}
			SQL.sendBatchOnMainThread(batch);
		}
		if(playersExists || warpsExists) {
			printInfo("Uploading complete, please remove plugin after use");
		} else {
			printError("Please place warps and players files in plugins/WarpToSQL");
		}
	}
}