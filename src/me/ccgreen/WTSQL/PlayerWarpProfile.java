package me.ccgreen.WTSQL;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public class PlayerWarpProfile {

	private List<String> knownWarps = new ArrayList<String>();
	private WTSQLMain main;
	private Player player;

	public PlayerWarpProfile(WTSQLMain main, Player player) {
		this.player = player;
		this.main = main;
		String uuid = player.getUniqueId().toString();
		try {
			if (main.playersYml.getConfigurationSection("Players").getKeys(false).contains(uuid)) {
				knownWarps = main.playersYml.getStringList("Players." + uuid);
			}
		} catch (NullPointerException e) {
			//File doesn't exist yet
		}
	}

	public void save() {
		main.playersYml.set("Players." + player.getUniqueId().toString(), knownWarps);
	}

	public List<String> getKnownWarps() {
		return knownWarps;
	}

}